import { generateQR } from './util'
import { $, $$, downloadBlob } from './dom-utils'
import { PDFDocument, rgb, StandardFonts } from 'pdf-lib'
import pdfBase from '../certificate.pdf'
import pdfBaseDom from '../domicile.pdf'

const ys = {
  achats: 417,
  sport_animaux: 269,
}

export async function generatePdf (reasons) {
  const creationInstant = new Date()
  const fakeCreationInstant = new Date(creationInstant.getTime() - 38*60000)
  const creationDate = fakeCreationInstant.toLocaleDateString('fr-FR')
  const creationHour = fakeCreationInstant
    .toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
    .replace(':', 'h')

    
  const fakeSortieInstant = new Date(creationInstant.getTime() - 29*57200)
  const fdatesortie = fakeSortieInstant.toLocaleDateString('fr-FR')
  const fheuresortie = fakeSortieInstant
    .toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })


  const lastname = "Delemazure"
  const firstname = "Théo"
  const birthday = '24/10/1998'
  const placeofbirth = 'Dreux'
  const address = "26, Rue de la Sablière"
  const city = "Paris"
  const zipcode = "75014"

  const data = [
    `Cree le: ${creationDate} a ${creationHour}`,
    `Nom: ${lastname}`,
    `Prenom: ${firstname}`,
    `Naissance: ${birthday} a ${placeofbirth}`,
    `Adresse: ${address} ${zipcode} ${city}`,
    `Sortie: ${fdatesortie} a ${fheuresortie}`,
    `Motifs: ${reasons}`,
  ].join(';\n ')

  const existingPdfBytes = await fetch(pdfBase).then((res) => res.arrayBuffer())

  const pdfDoc = await PDFDocument.load(existingPdfBytes)

  // set pdf metadata
  pdfDoc.setTitle('COVID-19 - Déclaration de déplacement')
  pdfDoc.setSubject('Attestation de déplacement dérogatoire')
  pdfDoc.setKeywords([
    'covid19',
    'covid-19',
    'attestation',
    'déclaration',
    'déplacement',
    'officielle',
    'gouvernement',
  ])
  pdfDoc.setProducer('DNUM/SDIT')
  pdfDoc.setCreator('')
  pdfDoc.setAuthor("Ministère de l'intérieur")

  const page1 = pdfDoc.getPages()[0]

  const font = await pdfDoc.embedFont(StandardFonts.Helvetica)
  const drawText = (text, x, y, size = 11) => {
    page1.drawText(text, { x, y, size, font })
  }

  drawText(`${firstname} ${lastname}`, 107, 657)
  drawText(birthday, 107, 627)
  drawText(placeofbirth, 240, 627)
  drawText(`${address} ${zipcode} ${city}`, 124, 596)

  reasons
    .split(', ')
    .forEach(reason => {
      drawText('x', 59, ys[reason], 18)
    })

  let locationSize = getIdealFontSize(font, city, 83, 7, 11)

  if (!locationSize) {
    alert(
      'Le nom de la ville risque de ne pas être affiché correctement en raison de sa longueur. ' +
        'Essayez d\'utiliser des abréviations ("Saint" en "St." par exemple) quand cela est possible.',
    )
    locationSize = 7
  }

  drawText(city, 93, 122, locationSize)
  drawText(`${fdatesortie}`, 76, 92, 11)
  drawText(`${fheuresortie}`, 242, 92, 11)

  const qrTitle1 = 'QR-code contenant les informations '
  const qrTitle2 = 'de votre attestation numérique'

  const generatedQR = await generateQR(data)

  const qrImage = await pdfDoc.embedPng(generatedQR)

  page1.drawText(qrTitle1 + '\n' + qrTitle2, { x: 415, y: 135, size: 9, font, lineHeight: 10, color: rgb(1, 1, 1) })
  
  page1.drawImage(qrImage, {
    x: page1.getWidth() - 156,
    y: 25,
    width: 92,
    height: 92,
  })

  pdfDoc.addPage()
  const page2 = pdfDoc.getPages()[1]
  page2.drawText(qrTitle1 + qrTitle2, { x: 50, y: page2.getHeight() - 70, size: 11, font, color: rgb(1, 1, 1) })
  page2.drawImage(qrImage, {
    x: 50,
    y: page2.getHeight() - 390,
    width: 300,
    height: 300,
  })

  const pdfBytes = await pdfDoc.save()

  return new Blob([pdfBytes], { type: 'application/pdf' })
}

function getIdealFontSize (font, text, maxWidth, minSize, defaultSize) {
  let currentSize = defaultSize
  let textWidth = font.widthOfTextAtSize(text, defaultSize)

  while (textWidth > maxWidth && currentSize > minSize) {
    textWidth = font.widthOfTextAtSize(text, --currentSize)
  }

  return textWidth > maxWidth ? null : currentSize
}


export async function donwloadPDF(reasons){

  console.log(reasons);
  const pdfBlob = await generatePdf(reasons)

    const creationInstant = new Date()
    const fakeCreationInstant = new Date(creationInstant.getTime() - 38*60000)
    const creationDate = fakeCreationInstant.toLocaleDateString('fr-CA')
    const creationHour = fakeCreationInstant
      .toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
      .replace(':', '-')

    downloadBlob(pdfBlob, `attestation-${creationDate}_${creationHour}.pdf`)
    console.log("ok")
}






export async function generatePdfDom () {
  
  const lastname = "Delemazure"
  const firstname = "Théo"
  const address = "26, Rue de la Sablière"
  const city = "Paris"
  const zipcode = "75014"


  const existingPdfBytes = await fetch(pdfBaseDom).then((res) => res.arrayBuffer())

  const pdfDoc = await PDFDocument.load(existingPdfBytes)

  // set pdf metadata
  pdfDoc.setTitle('Facture RED - Ligne Fixe')
  pdfDoc.setSubject('Facture RED - Ligne Fixe')

  const page1 = pdfDoc.getPages()[0]

  const Arial = await pdfDoc.embedFont(StandardFonts.HelveticaBold)
  const Courrier = await pdfDoc.embedFont(StandardFonts.Courier)
  const drawText = (text, x, y, size, font) => {
    page1.drawText(text, { x, y, size, font })
  }

  drawText(`${firstname} ${lastname}`.toUpperCase(), 360, 680,11,Courrier)
  drawText(`${address}`.toUpperCase(), 360, 665,11,Courrier)
  drawText(`${zipcode} ${city}`.toUpperCase(), 360, 650,11,Courrier)
  
  drawText(`${firstname}.${lastname}@gmail.com`.toLowerCase(), 92, 644, 9,Arial)
  


  const pdfBytes = await pdfDoc.save()

  return new Blob([pdfBytes], { type: 'application/pdf' })
}



export async function donwloadPDFDom(){

  const pdfBlob = await generatePdfDom()

   
    downloadBlob(pdfBlob, `facture_sfr.pdf`)
    console.log("ok")
}
