function createAndDownloadBlobFile(blob, filename = "justif") {//, extension = 'pdf'
    //   const blob = new Blob([body]);
    // const fileName = `justif.pdf`;
    // console.log("test gestion blob? CENSÉ ÊTRE APRÈS «JUSTE AVANT LE RETURN BLOB »")
    const fileName = `${filename}.pdf`;
    if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, fileName);
    } else {
        const link = document.createElement('a');
        // Browsers that support HTML5 download attribute
        if (link.download !== undefined) {
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', fileName);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}