
// alert("allloooooooooo")

async function refresh_position(rayon) {
    assetLayerGroup.clearLayers();
    $("#refresh_position").text("Récupération de la position en cours…");
    $("#adresse_générée").text("Cliquez sur « Générer une adresse »");
    $("#génération_adresse").text("Récupération des adresses en cours…");
    $("#btn_gen_pdf").text("Générer l’attestation");


    $("#btn_gen_pdf").prop("disabled", true);
    // console.log("map locate va être ppelée")
    await map_locate(rayon);
    
    // console.log(`draw on map va être appelée, ${user_location.latitude}`)
    draw_on_map();
    // console.log(`after calling map locate, value of user_location =${user_location.latitude}`);
    // let list_addresses = get_addresses(location,rayon);
    // console.log(res);
    let list_addresses = get_addresses(rayon)
    return list_addresses;
}


$("#refresh_position").click(async function () {
    // alert("test2")
    rayon = parseInt($("#rayon_distance").val());
    list_addresses = await refresh_position(rayon);
    // console.log("test");
    // console.log(list_addresses);
    $("#all_in_one_button").prop("disabled", false);

}
);

// on GÉNÉRATION ADRESSE click, DO
$("#génération_adresse").click(function () {
    full_address = select_address(list_addresses);

});

// ON BUTTON GÉNÉRER PDF, do: 
$("#btn_gen_pdf").click(async function () { await création_PDF(full_address.base_address, full_address.code_postal, full_address.ville) });


// $("#btn_toggle_hide").click(function () {
//     if ($("#btn_toggle_hide").val() === "hidden") {
//         $("#liste_adresses").show();
//         $("#btn_toggle_hide").val("shown");
//         $("#btn_toggle_hide").html("Cacher les adresses");
//     }
//     else {
//         $("#liste_adresses").hide();
//         $("#btn_toggle_hide").val("hidden");
//         $("#btn_toggle_hide").html("Montrer les adresses");
//     }
// });

$("#formulaire_info_persos").submit(function () {
    // alert("ça marche?")
    if ($('#subscribeNews').is(':checked')) {
        Cookies.set('nom_de_famille', $("#formulaire_info_persos input[name=nom_de_famille]").val(), { expires: 365, secure: 'true', });
        Cookies.set('prénom', $("#formulaire_info_persos input[name=prénom]").val(), { expires: 365, secure: 'true', });
        Cookies.set('lieu_de_naissance', $("#formulaire_info_persos input[name=lieu_de_naissance]").val(), { expires: 365, secure: 'true', });
        Cookies.set('date_de_naissance', $("#formulaire_info_persos input[name=date_de_naissance]").val(), { expires: 365, secure: 'true', });

    } else {
        Cookies.remove('nom_de_famille', { secure: true });
        Cookies.remove('prénom', { secure: true });
        Cookies.remove('lieu_de_naissance', { secure: true });
        Cookies.remove('date_de_naissance', { secure: true });
    }
    $("#formulaire_info_persos").css("background-color", "rgb(38, 174, 0)");
    $("#formulaire_info_persos")
        .animate({
            backgroundColor: "#121212"
        }, 800);
    return false;//pour ne pas changer de page après validation du formulaire
});

function select_address(list_addresses) {
    // alert("début select address")
    // console.log("select adresse en cours")
    $("#btn_gen_pdf").text("Générer l’attestation");
    // SÉLECTIONNER UNE ADRESSE AU HASARD DANS LA LISTE

    randomnumber = random_number_generator(0, list_addresses.length);//$("#liste_adresses li").length


    const selected_address = list_addresses[randomnumber];

    // dict = {
    //     numero_voie: list_infos["numero_voie"],
    //     type_voie: list_infos["type_voie"],
    //     voie: list_infos["voie"],
    //     code_postal: list_infos["code_postal"],
    //     commune: list_infos["commune"],
    //     gps_lat: list_infos["lat"],
    //     gps_lon: list_infos["lon"],
    // };

    const base_address = `${selected_address["numero_voie"]} ${selected_address["type_voie"]} ${selected_address["voie"]}`;
    // console.log(`base address = ${base_address}`);
    const code_postal = selected_address["code_postal"];
    // console.log(`code postal = ${code_postal}`);
    const ville = selected_address["commune"];
    const formatted_address = base_address + `, ${code_postal} ${ville}`;

    $("#adresse_générée").html(formatted_address); // afficher l’adresse obtenue

    // reset le précédent fake marker
    layer_fake_address.clearLayers();
    // METTRE UN POINT SUR LA MAP 

    var marker_fake_address = L.marker([selected_address["gps_lat"], selected_address["gps_lon"]], { icon: greenIcon }).bindPopup('You live here :)');


    // L.marker([51.5, -0.09], { icon: greenIcon }).addTo(map);
    layer_fake_address.addLayer(marker_fake_address);
    $("#génération_adresse").html("Générer une autre adresse");
    $("#btn_gen_pdf").prop("disabled", false);

    return {
        base_address,
        code_postal,
        ville
    }
        ;
}


$("#all_in_one_button").click(async function () {
    rayon = parseInt($("#rayon_distance").val())
    // (new Promise(refresh_Pos))
    //     .then(select_Adress)
    //     .then(création_PDF)
    //     .catch((err) => console.log(`Saucisse: ${err.message}`));
    // list_addresses = await refresh_position(rayon);
    full_address = select_address(list_addresses);
    await création_PDF(full_address.base_address, full_address.code_postal, full_address.ville);
    // refresh_Pos(() => (select_Adress(() => création_PDF())));
    // (new Promise(refresh_Pos))
    //     .then(select_Adress)
    //     .then(création_PDF)
    //     .catch((err) => console.log(`Saucisse: ${err.message}`));
    // refresh_Pos().then(select_Adress).then(création_PDF).catch((err) => console.log(`Saucisse: ${err.message}`));
    // refresh_Pos()
    //     .then(select_Adress)
    //     .then(création_PDF)
    //     .catch((err) => console.log(`Saucisse: ${err.message}`));
    // await select_Adress();
    // await création_PDF();
})