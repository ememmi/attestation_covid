
// Creating a map object
// map = L.map('map', { doubleClickZoom: false }).locate({ setView: true, maxZoom: 16 });
let map = L.map('map', {
    center: [ 48.856065, 2.343501 ],
    zoom: 14,
    // layers: [grayscale, cities]
});
// Creating a Layer object
let layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

// Adding layer to the map
map.addLayer(layer);

let assetLayerGroup = new L.LayerGroup();
let layer_fake_address = new L.LayerGroup();

map.addLayer(assetLayerGroup);
map.addLayer(layer_fake_address);
// map.locate();

var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

async function get_addresses(rayon) {
    let list_addresses = [];
    list_addresses = await get_list_addresses(rayon);

    return list_addresses;
}

function draw_on_map() {
    // alert("test?")
    // console.log("in draw on map");
    // console.log(`user_location=${user_location.latitude},${user_location.longitude}`);
    delta_bounds = parseInt($("#rayon_distance").val()) / 1000 / 111;
    map.fitBounds([
        [user_location.latitude - delta_bounds, user_location.longitude - delta_bounds],
        [user_location.latitude + delta_bounds, user_location.longitude + delta_bounds]
    ]);

    var marker_current_pos = L.marker([user_location.latitude,user_location.longitude]).bindPopup('You are here :)');
    var circle_around_cur_pos = L.circle([user_location.latitude,user_location.longitude], rayon, {
        // radius: 1,
        weight: 1,
        color: 'blue',
        fillColor: '#7700ff',
        fillOpacity: 0.2
    });
    assetLayerGroup.addLayer(marker_current_pos);
    assetLayerGroup.addLayer(circle_around_cur_pos);
}

function map_locate(rayon) {
    return new Promise((resolve, reject) => {
        map.locate({ setView: true, watch: false }) /* This will return map so you can do chaining */
            .once('locationfound', function (e) {
                // map.zoom()
                user_location.latitude = e.latitude;
                user_location.longitude= e.longitude;
                // console.log(`user location in map locate = ${user_location}`);
                // map.addLayer(marker);
                // map.addLayer(circle);
                // alert("test")
                $("#refresh_position").html("Actualiser ma position");
                resolve(e);
            })
            .once('locationerror', function (e) {
                console.log(e);
                $("#refresh_position").html("Erreur lors de la récupération de la position");
                reject(e);
            });
    });
}